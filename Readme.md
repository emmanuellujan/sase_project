# Título

Implementación de una máquina sencilla y su sistema de entrada/salida en la placa Nexys3 

## Descripción

Proyecto final para el curso de grado/posgrado: Diseño de Sistemas con FPGA. 
Departamento de Computación. Facultad de Ciencias Exactas y Naturales. Universidad de Buenos Aires. 
Profersora: Dra. Patricia Borensztejn. 
Autores: estudiantes del curso. 
Año: 2016.

Presentado en "Simposio Argentino de Sistemas Embebidos (SASE) 2016". 
URL: www.sase.com.ar/2016/


